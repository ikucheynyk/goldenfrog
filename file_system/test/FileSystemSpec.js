'use strict';

/**
 * Tests covering basic filesystem features: read, delete, move, write, mkdir
 */
describe('FileSystem:', function() {

    beforeEach(module('FS'));

    var FileSystem, $timeout;
    beforeEach(inject(function(_FileSystem_, _$timeout_) {
        FileSystem = _FileSystem_;
        $timeout = _$timeout_;
        FileSystem.setFileStructure({
            root: true,
            type: 'folder',
            children: [
                {name: 'folder1', type:'folder', children:[
                    {name: 'folder2', type:'folder'},
                    {name: 'file.txt', type:'file', content: 'Text in the file'}
                ]}
            ]
        });
    }));

    it('can access text file content', function() {
        var file;
        FileSystem.read('/folder1/file.txt').then(function(node) {
            file = node;
        });
        $timeout.flush();
        expect(file.content).toBe('Text in the file');
    });

    it('can access children of the folder', function() {
        var folder;
        FileSystem.read('/folder1').then(function(node) {
            folder = node;
        });
        $timeout.flush();
        expect(folder.children).toBeDefined();
        expect(folder.children.length).toBe(2);
    });

    it('can delete node', function() {
        var deleted, folder;
        FileSystem.drop(['/folder1/file.txt']).then(function(deletedNodes) {
            deleted = deletedNodes;
        });
        FileSystem.read('/folder1').then(function(node) {
            folder = node;
        });
        $timeout.flush();
        expect(deleted.length).toBe(1);
        expect(deleted[0].name).toBe('file.txt');
        expect(folder.children.length).toBe(1);
    });

    it('can move file from one folder to another', function() {
        var moved, srcFolder, destFolder;
        // Move the nodes
        FileSystem.move(['/folder1/file.txt'], '/folder1/folder2').then(function(movedNodes) {
            moved = movedNodes;
        });
        // read the destination folder
        FileSystem.read('/folder1/folder2').then(function(node) {
            destFolder = node;
        });
        // read the source folder
        FileSystem.read('/folder1').then(function(node) {
            srcFolder = node;
        });
        $timeout.flush();
        expect(srcFolder.children.length).toBe(1);
        expect(destFolder.children).toBeDefined();
        expect(destFolder.children.length).toBe(1);
        expect(moved[0].name).toBe('file.txt');
    });

    it('can write into new file', function() {
        var node, node2;
        FileSystem.write('/folder1/file2.txt', 'some content').then(function(createdNode) {
            node = createdNode;
        });
        FileSystem.read('/folder1/file2.txt').then(function(newNode) {
            node2 = newNode;
        });
        $timeout.flush();
        expect(node).toBeDefined();
        expect(node2.content).toBe('some content');
    });

    it('can create new folder', function() {
        var node, node2;
        FileSystem.mkdir('/folder1/folder2/folder3').then(function(createdNode) {
            node = createdNode;
        });
        FileSystem.read('/folder1/folder2').then(function(newNode) {
            node2 = newNode;
        });
        $timeout.flush();
        expect(node).toBeDefined();
        expect(node2.children[0].name).toBe('folder3');
    });

});
