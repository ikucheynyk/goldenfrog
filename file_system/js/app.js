'use strict';

/**
 * Created by igor on 1/4/15.
 */
var FS = angular.module('FS', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.otherwise({
            templateUrl: 'list',
            controller: 'FileListController'
        })
    }])

    // This service is used as a clipboard to hold the selected nodes while transitioning from one route to another
    .service('Clipboard', function() {
        return {
            data: null
        }
    })

    // This is the only controller here, passing
    .controller('FileListController', ['$scope', 'FileSystem', '$location', 'Clipboard', function($scope, FileSystem, $location, Clipboard) {
        $scope.path = $location.path().replace(/\/$/,'');
        $scope.selected = {};
        $scope.clipboard = Clipboard;

        /**
         * Takes the route and tries to find coresponding node
         * if successfull - will render that node children in a table
         */
        $scope.loadCurrentLevel = function() {
            // Loads current level
            FileSystem.read($location.path() || '/').then(function(node) {
                $scope.node = node;
            }, function(error) {
                $scope.error = error;
            });
        };
        // Doing it initially and sometimes after modifications to keep list updated
        $scope.loadCurrentLevel();

        /**
         * Indicates if there are node selections, used in template to disable action buttons
         * @return {boolean}
         */
        $scope.someSelected = function() {
            return !Object.keys($scope.selected).filter(function(s){return $scope.selected[s]}).length;
        };

        /**
         * Deletes selected nodes
         */
        $scope.dropSelected = function() {
            var itemsToDelete = Object.keys($scope.selected)
                .filter(function(s){return $scope.selected[s]})
                .map(function(s) {return $scope.path + '/' + s});
            FileSystem.drop(itemsToDelete).then(function() {
                $scope.selected = {};
                $scope.loadCurrentLevel();
            }, function(error) {
                $scope.error = error;
            });
        };

        /**
         * Deletes single node by name located in current path
         * @param sName
         */
        $scope.drop = function(sName) {
            FileSystem.drop([$scope.path + '/' + sName]).then(function() {
                $scope.loadCurrentLevel();
            }, function(error) {
                $scope.error = error;
            });
        };

        /**
         * Stores the list of selected paths into "clipboard" service to be able to paste it later
         */
        $scope.copyToClipboard = function() {
            Clipboard.data = Object.keys($scope.selected)
                .filter(function(s){return $scope.selected[s]})
                .map(function(s) {return $scope.path + '/' + s});
            $scope.selected = {};
        };

        /**
         * Pastes from "clipboard" node paths into curent folder
         */
        $scope.pasteFromClipboard = function() {
            FileSystem.move(Clipboard.data, $location.path() || '/').then(function() {
                Clipboard.data = null;
                $scope.loadCurrentLevel();
            }, function(error) {
                $scope.error = error;
            });
        };

        /**
         * Creates the file
         * Prompts for file name and content
         * Sorry, didn't have enough time to create separate pages for it
         */
        $scope.createFile = function() {
            var sFileName = prompt('File name:');
            if(!sFileName) {
                return;
            }
            var sContent = prompt('File content:');
            if(!sContent) {
                return;
            }
            FileSystem.write($scope.path + '/' + sFileName, sContent).then(function() {
                $scope.loadCurrentLevel();
            }, function(error) {
                $scope.error = error;
            });
        };

        /**
         * Creates a folder
         */
        $scope.createFolder = function() {
            var sFolderName = prompt('Folder name:');
            if(!sFolderName) {
                return;
            }
            FileSystem.mkdir($scope.path + '/' + sFolderName).then(function() {
                $scope.loadCurrentLevel();
            }, function(error) {
                $scope.error = error;
            });
        };
    }]);