'use strict';

/**
 * File system API, can perform read/write/mkdir/drop actions
 * working with internal data, doesn't do any server communication, all data is local
 */
FS.service('FileSystem', ['$q', '$timeout', function($q, $timeout) {
    var self = this;

    /**
     * Fake file structure that we're working with
     * @type {*[]}
     * @private
     */
    var oFileStructure = {
        root: true,
        type: 'folder',
        children: [
            {name: 'music', type:'folder', children: [
                {name:'Jazz', type:'folder', children:[
                    {name: 'Chris Botti', type:'file', content:'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'},
                    {name: 'Louis Armstrong', type:'file', content: 'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'},
                    {name: 'Diana Crall', type:'file', content:'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'}
                ]},
                {name:'Rock', type:'folder', children: [
                    {name: 'AC/DC', type:'file', content: 'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'},
                    {name: 'Scorpions', type:'file', content:'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'}
                ]},
                {name:'Pop', type:'folder'},
            ]},
            {name: 'text files', type:'folder', children: [
                {name: 'file1.txt', type:'file', content: 'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'},
                {name: 'file2.txt', type:'file', content: 'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'},
                {name: 'file3.txt', type:'file', content: 'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'},
                {name: 'file4.txt', type:'file', content: 'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'},
                {name: 'file5.txt', type:'file', content: 'Lorem ipsum dolor sit amet, id wisi vocent duo, magna accusamus disputationi cu sit. In convenire voluptatum eos, novum recteque usu an. In quo assum possit expetendis, quod atqui duo an. Elit aperiri sit an, at nec omnis liberavisse, ea eum ridens electram. Ius an nominavi pericula sadipscing, eius eruditi patrioque ad cum, an vel dolore labore.'}
            ]},
        ]
    };

    /**
     * Recursively creates references to parent node in each node
     * @param node - Node to start recursion
     * @private
     */
    var createNodeRelations = function(node) {
        if(node.children) {
            node.children.forEach(function(item) {
                item.parent = node;
                createNodeRelations(item);
            });
        }
    };
    // Do it initially
    createNodeRelations(oFileStructure);

    /**
     * This is suppose to be
     * @param oFileStructure
     */
    this.setFileStructure = function(oStruct) {
        oFileStructure = oStruct;
        createNodeRelations(oFileStructure);
    };

    /**
     * Looking up the node by the path
     * @param sPath
     * @return {promise}
     */
    var findNodeByPath = function(sPath) {
        var deferred = $q.defer();
        $timeout(function() {
            var node = oFileStructure;
            var nodeFound = sPath=='/' || !sPath.replace(/^\//, '').split('/').some(function(name) {
                    node = node.children.filter(function(node) {
                        return node.name == name;
                    })[0];
                    return !node;
                });
            if(nodeFound) {
                // Clone before returning the data to avoid structure change outside
                deferred.resolve(node);
            } else {
                deferred.reject('Path not found');
            }
        });
        return deferred.promise;
    };

    /**
     * Opens the node for reading
     * @param sPath
     * @return {promise}
     */
    this.read = function(sPath) {
        return findNodeByPath(sPath);
    };

    /**
     * Creates a folder with the path provided
     * Every missing folder in that path will be created
     * @param sPath
     * @return {promise}
     */
    this.mkdir = function(sPath) {
        var deferred = $q.defer();
        $timeout(function(){
            sPath = sPath.replace(/^\//, '');
            var aFolderPath = sPath.split('/');
            var oFolder = oFileStructure;
            aFolderPath.forEach(function(name) {
                if(!oFolder.children) {
                    oFolder.children = [];
                }
                var node = oFolder.children.filter(function(node) {
                    return node.name == name;
                })[0];
                if(!node) {
                    node = {
                        name: name,
                        type: 'folder',
                        children: [],
                        parent: oFolder
                    };
                    oFolder.children.push(node);
                    oFolder = node;
                } else if(node.type==='folder') {
                    oFolder = node;
                } else {
                    deferred.reject('Could not create a folder `' + name + '`, this name is already used by file');
                }
            });
            deferred.resolve(oFolder);
        });
        return deferred.promise;
    };

    /**
     * Writes content into the file
     * Every missing folder in that path will be created
     * @param sPath
     * @param sContent
     * @return {promise}
     */
    this.write = function(sPath, sContent) {
        var deferred = $q.defer();
        var aFolderPath = sPath.split('/');
        var sFileName = aFolderPath.pop();
        self.mkdir(aFolderPath.join('/')).then(function(oFolder) {
            var oFile = oFolder.children.filter(function(n) {
                return n.name == sFileName;
            })[0];
            if(oFile && oFile.type=='folder') {
                deferred.reject('Could not create a file, the folder with the same name exists by this path');
            } else if(oFile) {
                oFile.content = sContent;
            } else {
                oFile = {
                    name: sFileName,
                    type: 'file',
                    parent: oFolder,
                    content: sContent
                };
                oFolder.children.push(oFile);
            }
            deferred.resolve(oFile);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    /**
     * Drops the list of nodes, paths have to be full
     * @param aPaths
     * @return {*}
     */
    this.drop = function(aPaths) {
        return $q.all(aPaths.map(function(sPath) {
            return findNodeByPath(sPath).then(function(node) {
                return node.parent.children.splice(node.parent.children.indexOf(node), 1);
            });
        })).then(function(res) {
            return res.map(function(item) {
                return item[0];
            });
        });
    };

    /**
     * Moves nodes into another folder
     * @param aPaths
     * @param sDestNode
     * @return {promise}
     */
    this.move = function(aPaths, sDestNode) {
        var deferred = $q.defer();
        this.drop(aPaths).then(function(aDetachedNodes) {
            findNodeByPath(sDestNode).then(function(oNode) {
                if(oNode.type==='folder') {
                    aDetachedNodes.forEach(function (n) {
                        n.parent = oNode;
                        if (!oNode.children) {
                            oNode.children = [];
                        }
                        oNode.children.push(n);
                    });
                    deferred.resolve(aDetachedNodes);
                } else {
                    deferred.reject('Could not move the file, destination node is not a folder');
                }
            }, function(error) {
                deferred.reject(error);
            });
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

}]);